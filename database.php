<?php

//класс с набором бд-функций
class DB {
    public function DB() {  //конструктор (установление соединения с базой + установка кодировки символов)
        header("Content-Type: text/html; charset=UTF-8");
        $connection = mysql_connect('localhost', 'root', '');  //соединение
        mysql_select_db('newsarchive');
        mysql_set_charset('utf8', $connection);  //кодировка
        return true;
    }

    public function processRowSet($rowSet, $singleRow = false) {  //вывод ассоциативного массива результата запросов
        $resultArray = array();
        while ($row = mysql_fetch_assoc($rowSet)) {
            array_push($resultArray, $row);
        }
        if ($singleRow === true) {
            return $resultArray[0];
        }
        return $resultArray;
    }

    public function select($table) {  //полная выборка из таблицы table
        $sql = "SELECT * FROM " . $table;
        $result = mysql_query($sql);

        if (mysql_num_rows($result) == 1)
            return $this->processRowSet($result, true);
        return $this->processRowSet($result);
    }

    public function selectArrival($table, $ID) {  //выборка новости
        $sql = "SELECT * FROM " . $table . " WHERE `ARTICLE ID` = " . $ID;
        $result = mysql_query($sql);

        if (mysql_num_rows($result) == 1)
            return $this->processRowSet($result, true);
        return $this->processRowSet($result);
    }
    
    public function userInfo($AUTHOR) {  //выборка пользователя
        $sql = "SELECT * FROM `editorial office users` WHERE `AUTHOR` = '" . $AUTHOR."'";
        $result = mysql_query($sql);

        if (mysql_num_rows($result) == 1)
            return $this->processRowSet($result, true);
        return $this->processRowSet($result);
    }

    public function verify($AUTHOR, $PASSWORD) {  //верификация введенных данных пользователя (логин + пароль)
        $sql = 'SELECT * FROM `editorial office users` WHERE AUTHOR = "' . $AUTHOR . '" AND PASSWORD = "' . $PASSWORD . '"';
        $result = mysql_query($sql);

        if (mysql_num_rows($result) == 0) {  //если запрос вывел ноль строк в качестве результата...
            return -1;  //...запрет авторизации
        } else {
            return 1;
        }
    }

    public function SetOnline($AUTHOR) {  //установка флага использования данного аккаунта
        $sql = "UPDATE `editorial office users` SET ONLINE = TRUE WHERE AUTHOR = '" . $AUTHOR . "'";
        mysql_query($sql) or die(mysql_error());
    }

    public function SetOffline($AUTHOR) {  //удаление флага использования данного аккаунта
        $sql = "UPDATE `editorial office users` SET ONLINE = FALSE WHERE AUTHOR = '" . $AUTHOR . "'";
        mysql_query($sql) or die(mysql_error());  //выполнить запрос или вывести ошибку sql запроса
    }

    public function Insert_EOU($AUTHOR, $PASSWORD, $PRIVELEGE) {  //зарегестрировать пользователя
        $sql = 'INSERT INTO `newsarchive`.`editorial office users` (`ID`, `AUTHOR`, `PASSWORD`, `PRIVELEGE`) VALUES (NULL, "' . $AUTHOR . '", "' . $PASSWORD . '", "' . $PRIVELEGE . '")';
        mysql_query($sql) or die(mysql_error());
        return mysql_insert_id();
    }

    public function Insert_Article($NAME, $AUTHOR, $DESCRIPTION, $TOPIC, $COUNTRY, $PICTURE, $BODY) {  //вставить статью
        $sql = "INSERT INTO `newsarchive`.`articles` (`ARTICLE ID`, `NAME`, `AUTHOR`, `DESCRIPTION`, `TOPIC`, `COUNTRY`, `PICTURE`, `BODY`) VALUES (NULL, '$NAME', '$AUTHOR', '$DESCRIPTION', '$TOPIC', '$COUNTRY', '$PICTURE', '$BODY')";
        mysql_query($sql) or die(mysql_error());
        return mysql_insert_id();
    }

    public function ArrivalInPublic($ID) {  //опубликовать статью
        $sql = "UPDATE `articles` SET `PUBLIC` = '1' WHERE `ARTICLE ID` = " . $ID;
        mysql_query($sql) or die(mysql_error());
        return 1;
    }

    public function ArrivalInTrash($ID) {  //скрыть статью
        $sql = "UPDATE `articles` SET `PUBLIC` = '0' WHERE `ARTICLE ID` = " . $ID;
        mysql_query($sql) or die(mysql_error());
        return 1;
    }

    public function deleteComment($ID, $table) {  //удалить кортеж из таблицы $table
        $sql = 'DELETE FROM ' . $table . ' WHERE `COMMENT_ID` = "' . $ID . '"';
        mysql_query($sql) or die(mysql_error());
    }

    public function updArrival($ID, $AUTHOR, $NAME, $DESCRIPTION, $TOPIC, $COUNTRY, $PICTURE, $BODY) {  //обновить статью
        $sql = "UPDATE `newsarchive`.`articles` SET `AUTHOR` = '" . $AUTHOR . "', `NAME` = '" . $NAME . "', `DESCRIPTION` = '" . $DESCRIPTION . "', `TOPIC` = '" . $TOPIC . "', `COUNTRY` = '" . $COUNTRY . "', `PICTURE` = '" . $PICTURE . "', `BODY` = '" . $BODY . "' WHERE `articles`.`ARTICLE ID` = " . $ID;
        echo $sql;
        mysql_query($sql) or die(mysql_error());
    }

    public function selectComments($ID) {  //выбрать комментарии для определенной статьи
        $sql = "SELECT * FROM comments WHERE `ARTICLE_ID` = " . $ID;
        $result = mysql_query($sql);

        if (mysql_num_rows($result) == 1) {
            return $this->processRowSet($result, true);
        }
        return $this->processRowSet($result);
    }
    
    public function userArticles($AUTHOR){
        $sql = "SELECT * FROM articles WHERE `AUTHOR` = '" .$AUTHOR. "'";
        $result = mysql_query($sql);

        if (mysql_num_rows($result) == 1) {
            return $this->processRowSet($result, true);
        }
        return $this->processRowSet($result);
    }

}
<!--Страница редактирования статей-->
<html>
    <body>
        <?php
        include 'view.php';
        $viewCls = new View();
        $viewCls->menu();
        ?>
        <div id="workspace">
            <?php
            include 'database.php';
            $db = new DB();
            if ($_SESSION['accessLevel'] == 0) {
                $NewsArray = $db->userArticles($_SESSION['username']);
            } else {
                $NewsArray = $db->select('articles');
            }
            if ($NewsArray == null) {
                ?>
                <div style="position:absolute; top:45%; left:35%">
                    <h1>Arrival List Is Empty...</h1>
                </div>
                <?php
            }
            $viewCls->ArrivalShort_ForEdit($NewsArray);
            ?>
        </div>
    </body>
</html>
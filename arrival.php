<?php
//Страница со статьей
include 'database.php';
include 'view.php';
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Add Arrival</title>
    </head>
    <body>
        <?php
        session_start();
        $viewCls = new View();
        $viewCls->menu();
        ?>
        <div id="workspace">
            <?php
            $db = new DB();
            $NewsArray = $db->selectArrival('articles', $_GET["ID"]);
            $viewCls->ArrivalFull($NewsArray);
            ?>
            <br><br>Comments:
            <?php
            $CommentsArray = $db->selectComments($_GET["ID"]);
            $viewCls->CommentsSection($CommentsArray);
            ?>
        </div>
    </body>
</html>
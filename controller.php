<?php
//описание реагирования веб-сервера на отправку определенных форм
include 'database.php';

$db = new DB();

if(isset($_GET['ajAdd'])) {  //AJAX обработка запроса вставки статьи
    session_start();  //начало сессии
    $db->Insert_Article($_GET['aN'], $_GET['a'], $_GET['d'], $_GET['t'], $_GET['c'], $_GET['p'], $_GET['b']);
}

if (isset($_POST['reg'])) {  //при нажатии кнопки "регистрация"
    $AUTHOR = $_POST['Username'];
    $PASSWORD = $_POST['Password'];
    if (isset($_POST['Privelege'])) {
        $PRIVELEGE = 1;
    } else {
        $PRIVELEGE = 0;
    }

    $db->Insert_EOU($AUTHOR, $PASSWORD, $PRIVELEGE);
    header("Location: main.php");
    //echo("<script>location.href='index.php'</script>");
} else if (isset($_POST['insert'])) {  //при нажатии кнопки "вставить статью"
    session_start();  //начало сессии
    $db->Insert_Article($_POST['artName'], $_SESSION['username'], $_POST['description'], $_POST['topic'], $_POST['country'], $_POST['picture'], $_POST['bodyText']);
    header("Location: main.php");
    //echo("<script>location.href='main.php'</script>");
} else if (isset($_POST['log'])) {  //при нажатии кнопки "вход"
    session_start();
    $_SESSION['username'] = $_POST['Username'];

    $AUTHOR = $_POST['Username'];
    $PASSWORD = $_POST['Password'];

    if ($db->verify($AUTHOR, $PASSWORD) == 1) {  //при успешной авторизации...
        ?>
        <p align = "center">Access granted to <?php echo $_SESSION['username'] ?>...</p>
        <?php
        $info = $db->userInfo($_SESSION['username']);
        $_SESSION['accessLevel'] = $info['PRIVELEGE'];
        $db->SetOnline($AUTHOR);
        header("Location: main.php");
        //echo("<script>location.href='usrMainMenu.php'</script>");
    } else {  //в противном случае...
        ?>
        <p align = "center">Access restricted...</p>
        <?php
        unset($_SESSION['username']);
        session_destroy();  //завершить сессию
        header("Location: index.php");
        //echo("<script>location.href='index.php'</script>");
    }
} else if (isset($_POST['logout'])) {  //при нажатии кнопки "выход"
    session_start();
    $db->SetOffline($_SESSION['username']);
    unset($_SESSION['username']);
    session_destroy();
    header("Location: index.php");
    //echo("<script>location.href='index.php'</script>");
} else if (isset($_POST['publicate'])) {  //при нажатии кнопки "публикация"
    $db->ArrivalInPublic($_POST['publicate']);
    header("Location: News.php");
    //echo("<script>location.href='News.php'</script>");
} else if (isset($_POST['hide'])) {  //при нажатии кнопки "скрыть новость"
    $db->ArrivalInTrash($_POST['hide']);
    header("Location: News.php");
    //echo("<script>location.href='News.php'</script>");
} else if (isset($_POST['edit'])) {  //при нажатии кнопки "редактировать новость"
    session_start();
    $db->updArrival($_POST['ID'], $_SESSION['username'], $_POST['artName'], $_POST['description'], $_POST['topic'], $_POST['country'], $_POST['picture'], $_POST['bodyText']);
    echo("<script>location.href='main.php'</script>");
} else if (isset($_POST['delComment'])) {
    $db->deleteComment($_POST['delComment'], "comments");
    echo("<script>history.back()</script>");
}
?>
<!--Страница добавления-->
<html>
    <head>
        <?php
            include 'view.php';
            $viewCls = new View();
        ?>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Add Arrival</title>
    </head>
    <body>
        <?php
            $viewCls->menu();
        ?>
        <div id="workspace">
        <?php
        $viewCls->arrivalInsertion();
        ?>
        </div>
    </body>
</html>
$(document).ready(function () {
    $("#showMenu").click(function () {
        $("#mBody").show();
        $("#mBody").animate({
            opacity: '0.6' 
        });
        
        $("#showMenu").animate({
            left: '-5%'
        });
        
        $("#mTable").show();
        
        $("#mbg").animate({
            left: '-25%'
        });
        
        $("#menu").animate({
            opacity: '1',
            left: '0%'
        });
        
        $("#hButton").show("slow");
        $("#hButton").animate({
            left: '70%'
        });
        
        $("#b1, #b2, #b3, #b4").show("slow");
    });

    $("#hButton").click(function () {
        $("#b1, #b2, #b3, #b4").hide("slow");
        
        $("#hButton").animate({
            left: '70%'
        });
        $("#hButton").hide("slow");
        
        $("#mTable").hide("slow");
        
        $("#menu").animate({
            left: '-100%',
            opacity: '0'
        });
        
        $("#mbg").animate({
            left: '-100%'
        });
        
        $("#showMenu").animate({
            left: '0%'
        });
        
        $("#mBody").animate({
            opacity: '0' 
        });
        $("#mBody").hide();
    });
    
    $("#showMenu").mouseover(function () {
        $("#showMenu").css("background-color", "blue");
    });
    
    $("#showMenu").mouseout(function () {
        $("#showMenu").css("background-color", "transparent");
    });

});



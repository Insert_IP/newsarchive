<!--список статей-->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Add Arrival</title>
        
    </head>
    <body>
        <?php
        include 'view.php';
        include 'database.php';
        session_start();
        $viewCls = new View();
        $db = new DB();
        if ($_SESSION['accessLevel'] == 0) {
            $NewsArray = $db->userArticles($_SESSION['username']);
        } else {
            $NewsArray = $db->select('articles');
        }
        if ($NewsArray == null) {
            ?>
            <div style="position:absolute; top:45%; left:35%">
                <h1>Arrival List Is Empty...</h1>
            </div>
            <?php
        }
        
            $viewCls->menu();
        ?>
        <div id="workspace">
            <?php
            $viewCls->ArrivalShort($NewsArray);
            ?>
        </div>
    </body>
</html>
<script>
    function validateForm() {
        var x = document.forms["validate"]["Username"].value;

        if (x === null || x === "") {
            alert("Name must be filled out");
            return false;
        }

    }
</script>

<?php

//описание визуальных объектов на сайте
class View {

    function ViewM() {  //меню
        echo "<p><a href = 'index.php?'>Главная</a></p>";
        echo "<p><a href = 'edit.php'>Редактировать</a></p>";
        echo "<p><a href = 'insert.php'>Удалить</a></p>";
    }

    function arrivalInsertion() {  //форма вставки статьи
        ?>
        <script src="jquery-1.11.3.js"></script>
        <script src ="newsAdd.js"></script>
        <link rel="stylesheet" type="text/css" href="formTables.css">
        <form>
            <table>
                <tr>
                    <td><p>Author:</p></td><td><input disabled type = "text" name = "auth" value = "<?php echo $_SESSION['username']; ?>"</td>
                </tr>
                <tr>
                    <td>Article Name:</td><td><input type = "text" name = "artName" size = 126></td>
                </tr>
                <td>Description:</td><td><input type = "text" name = "description" size = 126></td>
                </tr>
                <td>Topic:</td>
                <td>
                    <select name = "topic">
                        <option disabled>Choose Option</option>
                        <option value = "Policy">Policy</option>
                        <option value = "Economy">Economy</option>
                        <option value = "Culture">Culture</option>
                        <option value = "Sport">Sport</option>
                        <select>
                            </td>
                            </tr>
                            <td>Country:</td><td><input type = "text" name = "country" size = 126></td>
                            </tr>
                            <tr>
                                <td>Picture:</td><td><input type = "text" name = "picture" size = 126></td>
                            </tr>
                            <tr>
                                <td>Body:</td><td><textarea name = "bodyText" cols = 128 rows = 27></textarea></td>
                            </tr>
                            </table>
                            <p align = "center"><input type = "button" value = "Insert Arrival" name = "insert" onclick="insertArrival(this.form)"></p>
                            </form>
                            <?php
                        }

                        function arrivalEdition($arr) {  //форма редактирования статьи
                            ?>
                            <link rel="stylesheet" type="text/css" href="formTables.css">
                            <form action = "controller.php" method = "post">
                                <table border="1" align = "center">
                                    <tr>
                                        <td>Author:</td><td><?php echo $_SESSION['username']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Article ID:</td><td><input type = "text" name = "ID" value = <?php echo $arr['ARTICLE ID']; ?> readonly></td>
                                    </tr>
                                    <tr>
                                        <td>Article Name:</td><td><input type = "text" name = "artName" size = 126 value = "<?php echo $arr['NAME'] ?>"></td>
                                    </tr>
                                    <td>Description:</td><td><input type = "text" name = "description" size = 126 value = "<?php echo $arr['DESCRIPTION'] ?>"></td>
                                    </tr>
                                    <td>Topic:</td>
                                    <td>
                                        <select name = "topic">
                                            <option disabled>Choose Option</option>
                                            <option value = "Policy">Policy</option>
                                            <option value = "Economy">Economy</option>
                                            <option value = "Culture">Culture</option>
                                            <option value = "Sport">Sport</option>
                                            <select>
                                                </td>
                                                </tr>
                                                <td>Country:</td><td><input type = "text" name = "country" size = 126 value = "<?php echo $arr['COUNTRY'] ?>"></td>
                                                </tr>
                                                <tr>
                                                    <td>Picture:</td><td><input type = "text" name = "picture" size = 126 value = "<?php echo $arr['PICTURE'] ?>"></td>
                                                </tr>
                                                <tr>
                                                    <td>Body:</td><td><textarea name = "bodyText" rows = 27 cols = 128><?php echo $arr['BODY'] ?></textarea></td>
                                                </tr>
                                                </table>
                                                <p align = "center"><input type = "submit" value = "Edit Arrival" name = "edit"></p>
                                                <?php
                                            }

                                            function UserWorkSpace() {  //описание расположения фреймов
                                                session_start();
                                                ?>
                                                <FRAMESET cols="20%, 80%">
                                                    <FRAME src="menu.php" name = "one">
                                                        <FRAME src="main.php" name = "two">
                                                            </FRAMESET>
                                                            <?php
                                                        }

                                                        function showLogOut() {  //кнопка выход
                                                            ?>
                                                            <form action = "controller.php" method = "post" target = "_top">
                                                                <p align = "center"><input type = "submit" value = "LogOut" name = "logout"></p>
                                                                <?php
                                                            }

                                                            function showLogin() {  //форма авторизации
                                                                ?>
                                                                <form name = "validate" onsubmit = "return validateForm()" action = "controller.php" method = "post">
                                                                    <p align = "center">Username: <input type="text" name="Username"></p>
                                                                    <p align = "center">Password: <input type="password" name="Password"></p>
                                                                    <p align = "center"><input type = "submit" value = "Login" name = "log"></p>
                                                                    <?php
                                                                }

                                                                function showRegistration() {  //форма регистрации
                                                                    ?>
                                                                    <form action = "controller.php" method = "post">
                                                                        <p align = "center">Username: <input type="text" name="Username"></p>
                                                                        <p align = "center">Password: <input type="text" name="Password"></p>
                                                                        <p align = "center">Admin Rights: <input type="checkbox" name="Privelege" value = 1></p>
                                                                        <p align = "center"><input type = "submit" value = "Confirm Registration" name = "reg"></p>
                                                                        <?php
                                                                    }

                                                                    function ArrivalShort($arr) {  //краткое описание новости
                                                                        if (isset($arr['ARTICLE ID'])) {
                                                                            ?>
                                                                            <table width = "100%" border = 1 onmouseover= "style.backgroundColor = 'blue'" onmouseout= "style.backgroundColor = 'white'" onclick = "location.href = 'arrival.php?ID=' + <?php echo $arr[$i]['ARTICLE ID'] ?>">

                                                                                <tr>
                                                                                    <?php
                                                                                    echo "<td width = 10%>" . $arr['TOPIC'] . "</td>";
                                                                                    echo "<td valign = middle><h3><a href = 'arrival.php?ID=" . $arr['ARTICLE ID'] . "'>" . $arr['NAME'] . "</a></h3></td>";
                                                                                    ?>

                                                                                    <?php
                                                                                    if ($_SESSION['accessLevel'] == 1) {
                                                                                        if ($arr['PUBLIC'] == 0) {
                                                                                            ?>
                                                                                            <td width = 10% align = "middle">
                                                                                                <form action = "controller.php" method = "post">
                                                                                                    <button name = "publicate" type = "submit" value = <?php echo $arr['ARTICLE ID'] ?>>Publication</button>
                                                                                                </form>
                                                                                            </td>
                                                                                            <?php
                                                                                        } else {
                                                                                            ?>
                                                                                            <td width = 10% align = "middle">
                                                                                                <form action = "controller.php" method = "post">
                                                                                                    <button name = "hide" type = "submit" value = <?php echo $arr['ARTICLE ID'] ?>>Hide</button>
                                                                                                </form>
                                                                                            </td>
                                                                                            <?php
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                </tr>

                                                                                <table width = "100%" border = 1 onmouseover= "style.backgroundColor = 'blue'" onmouseout= "style.backgroundColor = 'white'" onclick = "location.href = 'arrival.php?ID=' + <?php echo $arr[$i]['ARTICLE ID'] ?>">
                                                                                    <?php
                                                                                    echo "<td>" . $arr['DESCRIPTION'] . "</td>";
                                                                                    ?>
                                                                                </table>

                                                                            </table>
                                                                            <br>
                                                                            <?php
                                                                        } else {
                                                                            for ($i = 0; $i < count($arr); $i++) {
                                                                                ?>
                                                                                <table width = "100%" border = 1 onmouseover= "style.backgroundColor = 'blue'" onmouseout= "style.backgroundColor = 'white'" onclick = "location.href = 'arrival.php?ID=' + <?php echo $arr[$i]['ARTICLE ID'] ?>">

                                                                                    <tr>
                                                                                        <?php
                                                                                        echo "<td width = 10%>" . $arr[$i]['TOPIC'] . "</td>";
                                                                                        echo "<td valign = middle><h3>" . $arr[$i]['NAME'] . "</h3></td>";
                                                                                        ?>

                                                                                        <?php
                                                                                        if ($_SESSION['accessLevel'] == 1) {
                                                                                            if ($arr[$i]['PUBLIC'] == 0) {
                                                                                                ?>
                                                                                                <td width = 10% align = "middle">
                                                                                                    <form action = "controller.php" method = "post">
                                                                                                        <button name = "publicate" type = "submit" value = <?php echo $arr[$i]['ARTICLE ID'] ?>>Publication</button>
                                                                                                    </form>
                                                                                                </td>
                                                                                                <?php
                                                                                            } else {
                                                                                                ?>
                                                                                                <td width = 10% align = "middle">
                                                                                                    <form action = "controller.php" method = "post">
                                                                                                        <button name = "hide" type = "submit" value = <?php echo $arr[$i]['ARTICLE ID'] ?>>Hide</button>
                                                                                                    </form>
                                                                                                </td>
                                                                                                <?php
                                                                                            }
                                                                                        }
                                                                                        ?>
                                                                                    </tr>

                                                                                    <table width = "100%" border = 1 onmouseover="style.backgroundColor = 'blue'" onmouseout="style.backgroundColor = 'white'" onclick = "location.href = 'edit.php?ID=' + <?php echo $arr[$i]['ARTICLE ID'] ?>">
                                                                                        <?php
                                                                                        echo "<td>" . $arr[$i]['DESCRIPTION'] . "</td>";
                                                                                        ?>
                                                                                    </table>

                                                                                </table>
                                                                                <br>
                                                                                <?php
                                                                            }
                                                                        }
                                                                    }

                                                                    function ArrivalShort_ForEdit($arr) {  //краткое описание новости для редактирования
                                                                        if (isset($arr['ARTICLE ID'])) {
                                                                            ?>
                                                                            <table width = "100%" border = 1 onmouseover= "style.backgroundColor = 'blue'" onmouseout= "style.backgroundColor = 'white'" onclick = "location.href = 'edit.php?ID=' + <?php echo $arr[$i]['ARTICLE ID'] ?>">

                                                                                <tr>
                                                                                    <?php
                                                                                    echo "<td width = 10%>" . $arr['TOPIC'] . "</td>";
                                                                                    echo "<td valign = middle><h3><a href = 'edit.php?ID=" . $arr['ARTICLE ID'] . "'>" . $arr['NAME'] . "</a></h3></td>";
                                                                                    ?>

                                                                                    <?php
                                                                                    if ($_SESSION['accessLevel']) {
                                                                                        if ($arr['PUBLIC'] == 0) {  //выбор кнопки в зависимости от статуса статьи (опубликована/не опубликована)
                                                                                            ?>
                                                                                            <td width = 10% align = "middle">
                                                                                                <form action = "controller.php" method = "post">
                                                                                                    <button name = "publicate" type = "submit" value = <?php echo $arr['ARTICLE ID'] ?>>Publication</button>
                                                                                                </form>
                                                                                            </td>
                                                                                            <?php
                                                                                        } else {
                                                                                            ?>
                                                                                            <td width = 10% align = "middle">
                                                                                                <form action = "controller.php" method = "post">
                                                                                                    <button name = "hide" type = "submit" value = <?php echo $arr['ARTICLE ID'] ?>>Hide</button>
                                                                                                </form>
                                                                                            </td>
                                                                                            <?php
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                </tr>

                                                                                <table width = "100%" border = 1 onmouseover= "style.backgroundColor = 'blue'" onmouseout= "style.backgroundColor = 'white'" onclick = "location.href = 'edit.php?ID=' + <?php echo $arr[$i]['ARTICLE ID'] ?>">
                                                                                    <?php
                                                                                    echo "<td>" . $arr['DESCRIPTION'] . "</td>";
                                                                                    ?>
                                                                                </table>

                                                                            </table>
                                                                            <br>
                                                                            <?php
                                                                        } else {
                                                                            for ($i = 0; $i < count($arr); $i++) {
                                                                                ?>
                                                                                <table width = "100%" border = 1 onmouseover= "style.backgroundColor = 'blue'" onmouseout= "style.backgroundColor = 'white'" onclick = "location.href = 'edit.php?ID=' + <?php echo $arr[$i]['ARTICLE ID'] ?>">

                                                                                    <tr>
                                                                                        <?php
                                                                                        echo "<td width = 10%>" . $arr[$i]['TOPIC'] . "</td>";
                                                                                        echo "<td valign = middle><h3><a href = 'edit.php?ID=" . $arr[$i]['ARTICLE ID'] . "'>" . $arr[$i]['NAME'] . "</a></h3></td>";
                                                                                        ?>

                                                                                        <?php
                                                                                        if ($_SESSION['accessLevel'] == 1) {
                                                                                            if ($arr[$i]['PUBLIC'] == 0) {  //выбор кнопки в зависимости от статуса статьи (опубликована/не опубликована)
                                                                                                ?>
                                                                                                <td width = 10% align = "middle">
                                                                                                    <form action = "controller.php" method = "post">
                                                                                                        <button name = "publicate" type = "submit" value = <?php echo $arr[$i]['ARTICLE ID'] ?>>Publication</button>
                                                                                                    </form>
                                                                                                </td>
                                                                                                <?php
                                                                                            } else {
                                                                                                ?>
                                                                                                <td width = 10% align = "middle">
                                                                                                    <form action = "controller.php" method = "post">
                                                                                                        <button name = "hide" type = "submit" value = <?php echo $arr[$i]['ARTICLE ID'] ?>>Hide</button>
                                                                                                    </form>
                                                                                                </td>
                                                                                                <?php
                                                                                            }
                                                                                        }
                                                                                        ?>
                                                                                    </tr>

                                                                                    <table width = "100%" border = 1 onmouseover= "style.backgroundColor = 'blue'" onmouseout= "style.backgroundColor = 'white'" onclick = "location.href = 'arrival.php?ID=' + <?php echo $arr[$i]['ARTICLE ID'] ?>">
                                                                                        <?php
                                                                                        echo "<td>" . $arr[$i]['DESCRIPTION'] . "</td>";
                                                                                        ?>
                                                                                    </table>

                                                                                </table>
                                                                                <br>
                                                                                <?php
                                                                            }
                                                                        }
                                                                    }

                                                                    function ArrivalFull($arr) {  //полная форма статьи
                                                                        echo "<h1>" . $arr['NAME'] . "</h1><br>";
                                                                        echo "<h3>by <I>" . $arr['AUTHOR'] . "</I></h3><br><br>";
                                                                        echo $arr['BODY'];
                                                                        if ($_SESSION['accessLevel'] == 1) {
                                                                            if ($arr['PUBLIC'] == 0) {
                                                                                ?>
                                                                                <form action = "controller.php" method = "post">
                                                                                    <p align = "middle"><button name = "publicate" type = "submit" value = <?php echo $arr['ARTICLE ID'] ?>>Publication</button></p>
                                                                                </form>
                                                                                <?php
                                                                            } else {
                                                                                ?>
                                                                                <form action = "controller.php" method = "post">
                                                                                    <p align = "middle"><button name = "hide" type = "submit" value = <?php echo $arr['ARTICLE ID'] ?>>Hide</button></p>
                                                                                </form>
                                                                                <?php
                                                                            }
                                                                        }
                                                                    }

                                                                    function CommentsSection($arr) {
                                                                        if (isset($arr['ARTICLE_ID'])) {
                                                                            ?>
                                                                            <table border ="1" width = 100%>
                                                                                <tr>
                                                                                    <td width = 10%>
                                                                                        <p align = "center"><?php echo $arr['USER']; ?></p>
                                                                                    </td>
                                                                                    <td width = 76%>
                                                                                        <?php echo $arr['BODY']; ?>
                                                                                    </td>
                                                                                    <td width = 8%>
                                                                                        <?php echo $arr['DATE']; ?>
                                                                                    </td>
                                                                                    <td width = 6%>
                                                                                        <?php
                                                                                        if ($_SESSION['accessLevel'] == 1) {
                                                                                            ?>
                                                                                            <form action = "controller.php" method = "post">
                                                                                                <p align = "center"><button name = "delComment" type = "submit" value = <?php echo $arr['COMMENT_ID'] ?>>Delete</button></p>
                                                                                            </form>
                                                                                        </td>
                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                </tr>
                                                                            </table>
                                                                            <?php
                                                                        } else {
                                                                            for ($i = 0; $i < count($arr); $i++) {
                                                                                ?>
                                                                                <table border ="1" width = 100%>
                                                                                    <tr>
                                                                                        <td width = 10%>
                                                                                            <p align = "center"><?php echo $arr[$i]['USER']; ?></p>
                                                                                        </td>
                                                                                        <td width = 76%>
                                                                                            <?php echo $arr[$i]['BODY']; ?>
                                                                                        </td>
                                                                                        <td width = 8%>
                                                                                            <?php echo $arr[$i]['DATE']; ?>
                                                                                        </td>
                                                                                        <?php
                                                                                        if ($_SESSION['accessLevel'] == 1) {
                                                                                            ?>
                                                                                            <td width = 6%>
                                                                                                <form action = "controller.php" method = "post">
                                                                                                    <button name = "delComment" type = "submit" value = <?php echo $arr[$i]['COMMENT_ID'] ?>>Delete</button>
                                                                                                </form>
                                                                                            </td>
                                                                                            <?php
                                                                                        }
                                                                                        ?>
                                                                                    </tr>
                                                                                </table>
                                                                                <?php
                                                                            }
                                                                        }
                                                                    }

                                                                    function menu() {
                                                                        ?>
                                                                        <link rel="stylesheet" type="text/css" href="shit.css">
                                                                        <script src="jquery-1.11.3.js"></script>
                                                                        <script src ="jqNews.js"></script>
                                                                        <div id="mBody"></div>
                                                                        <div id='showMenu'><br><div id="showMenuId">Menu</div></div>
                                                                        <img id = "hButton" src = "img/hide.png">
                                                                        <img id ="mbg" src="img/menu.png">
                                                                        <div id="menu">
                                                                            <br>
                                                                            <h2><a href = "add.php"><img class="mButton" id="b1" src="img/buttonAdd.png"></a></h2><br>
                                                                            <h2><a href = "News.php"><img class="mButton" id="b2" src="img/buttonList.png"></a></h2><br>
                                                                            <h2><a href = "editMenu.php"><img class="mButton" id="b3" src="img/buttonEdit.png"></a></h2><br>
                                                                            <?php
                                                                            session_start();
                                                                            if ($_SESSION['accessLevel'] == 1) {
                                                                                ?>
                                                                                <td width="25%"><h2><a href = "register.php" target = "two"><img id="b4" src="img/buttonRegister.png"></a></h2></td>
                                                                                            <?php
                                                                                        }
                                                                                        ?>
                                                                        </div>
                                                                        <?php
                                                                    }

                                                                }
                                                                ?>
<!--редактирование статьи-->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Edit Arrival</title>
    </head>
    <body>
        <?php
        include 'view.php';
        $viewCls = new View();
        $viewCls->menu();
        ?>
        <div id="workspace">
            <?php
            include 'database.php';
            $viewCls = new View();
            $db = new DB();
            $NewsArray = $db->selectArrival('articles', $_GET["ID"]);
            $viewCls->arrivalEdition($NewsArray);
            ?>
        </div>
    </body>
</html>